package ca.hank.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ca.hank.entity.Address;
import ca.hank.entity.Flight;
import ca.hank.entity.Passenger;

import ca.hank.exception.PassengerNotFoundException;

@RestController
@RequestMapping("/api")

public class PassengerRestController {

	public static Date StringToDate(String s) {

		Date date = null;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			date = dateFormat.parse(s);
		}

		catch (ParseException e) {
			e.printStackTrace();

		}
		return date;
	}

	public static Date StringToDate1(String s) {

		Date date = null;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			date = dateFormat.parse(s);
		}

		catch (ParseException e) {
			e.printStackTrace();

		}
		return date;
	}

	private List<Passenger> passengerList;
	private Date bd1 = StringToDate("10/12/1990");
	private Date bd2 = StringToDate("12/09/1986");
	private Date bd3 = StringToDate("15/06/1982");

	private Date dd1 = StringToDate("09/11/2019");
	private Date dd2 = StringToDate("07/06/2019");
	private Date dd3 = StringToDate("09/08/2019");

	private Date ad1 = StringToDate("10/12/2019");
	private Date ad2 = StringToDate("10/07/2019");
	private Date ad3 = StringToDate("21/09/2019");

	Flight f1 = new Flight("Montreal", dd1, "Toronto", ad1);
	Flight f2 = new Flight("Toronto", dd2, "Vancouver", ad2);
	Flight f3 = new Flight("Montreal", dd3, "Toronto", ad3);

	Address a1 = new Address(100, "Saint Catherine St", "Montreal", "Quebec", "H3H 282", "CA");
	Address a2 = new Address(1450, "Maisonneuve", "Montreal", "Quebec", "H3H 5g6", "CA");
	Address a3 = new Address(1000, "Yonge", "Toronto", "Quebec", "f6j 2g6", "CA");

	@PostConstruct
	public void LoadData() {

		passengerList = new ArrayList<>();

		passengerList.add(new Passenger("Sheldon", "Wouper", bd1, "s@s.com", "555-555-5555", "M", "01525", a1, f1));
		passengerList.add(new Passenger("Penny", "Rossi", bd2, "penny@s.com", "111-111-1111", "M", "21225", a2, f2));
		passengerList
				.add(new Passenger("Leonard", "Smith", bd3, "leonard@s.com", "222-222-2222", "M", "31122", a3, f3));

	}

	// define end point for "/passengers" - return list of passengers
	@GetMapping("/passengers")
	public List<Passenger> getPassengers() {
		return passengerList;
	}

	// show just one passenger by id
	@GetMapping("/passengers/{passengerId}")
	public Passenger getPassenger(@PathVariable int passengerId) {
		// 3-throw exception from rest controller method

		if ((passengerId >= passengerList.size()) || (passengerId < 0)) {
			throw new PassengerNotFoundException("Passenger Id not found - " + passengerId);
		}

		return passengerList.get(passengerId);
	}

	// find a passenger by family
	List<Passenger> passengerHasSameFamily = new ArrayList<Passenger>();
	@GetMapping("/passengers/find/family/{family}")
	public List<Passenger> getPassenger(@PathVariable("family") String family) {
		for (Passenger s : passengerList) {
			if (s.getFamily() != null && s.getFamily().toLowerCase().contains(family.toLowerCase())) {
				passengerHasSameFamily.add(s);
			}
		}
		return passengerHasSameFamily;
	
	}

	// show list of passengers ordered by family
	@GetMapping("/passengers/sort/family")
	public List<Passenger> getSortedPassengerByFamily() {
//		Collections.sort(passengerList);
//		return passengerList;

		Collections.sort(passengerList, new Comparator<Passenger>() {
			public int compare(Passenger p1, Passenger p2) {
				if (p1.getFamily() == null || p2.getFamily() == null) {
					throw new PassengerNotFoundException("Passenger Id not found - ");
				}
				return p1.getFamily().compareTo(p2.getFamily());
			}
		});
		return passengerList;
	}

	// show list of passengers whose address in same city
	List<Passenger> passengerAddressSameListCityList = new ArrayList<Passenger>();

	@GetMapping("/passengers/city/{city}")
	public List<Passenger> getPassengerInCity(@PathVariable("city") String city) {

		for (Passenger p : passengerList) {
			if (p.getAddress().getCity().toLowerCase().contains(city.toLowerCase())) {
				passengerAddressSameListCityList.add(p);
			}
		}
		return passengerAddressSameListCityList;

//		Iterator<Passenger> iterator = passengerList.iterator();
//		while (iterator.hasNext()) {
//			Passenger passenger = iterator.next();
//			if (passenger.getAddress().getCity().toLowerCase().contains(city.toLowerCase())) {
//				return passenger;
//			} else {
//				throw new PassengerNotFoundException("Passenger Id not found - ");
//			}
//		}
//		return null;

	}

	@GetMapping("/passengers/sort/departuredate")
	public List<Passenger> getsortedPassengersByDeparturedate() {
		Collections.sort(passengerList, new Comparator<Passenger>() {
			public int compare(Passenger p1, Passenger p2) {
				if (p1.getFlight().getDepartureDate() == null || p2.getFlight().getDepartureDate() == null) {

					throw new PassengerNotFoundException("Passenger Id not found - ");
				}
				return p1.getFlight().getDepartureDate().compareTo(p2.getFlight().getDepartureDate());
			}
		});
		return passengerList;
	}

	@GetMapping("/passengers/find/date/{date}/city/{city}")
	public Passenger getsortedPassengersByDeparturedateAndDestinationcity(
			@PathVariable("date") @DateTimeFormat(pattern = "yyyy-MM-dd") String date,
			@PathVariable("city") String city) {

		Date departureDate = StringToDate1(date);
		for (Passenger s : passengerList) {
			if (s.getFlight() != null) {
				if (s.getFlight().getDepartureDate().equals(departureDate)
						&& s.getFlight().getToCity().toLowerCase().equals(city.toLowerCase())) {
					return s;
				}
			}

			else {
				throw new PassengerNotFoundException("Passenger Id not found - ");
			}

		}
		return null;

	}

	@DeleteMapping("/passengers/delete/{passengerId}")
	public List<Passenger> DeletePassenger(@PathVariable int passengerId) {

		if ((passengerId >= passengerList.size()) || (passengerId < 0)) {
			throw new PassengerNotFoundException("Passenger id not found -  " + passengerId);
		}

		passengerList.remove(passengerId);
		return passengerList;
	}

}
