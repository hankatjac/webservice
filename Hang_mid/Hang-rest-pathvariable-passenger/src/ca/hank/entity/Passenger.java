package ca.hank.entity;

import java.util.Date;

public class Passenger {
	private String name;
	private String family;
	private Date birthDate;
	private String email;
	private String phone;
	private String gender;
	private String payment;
	private Address address;
	private Flight flight;

	public Passenger() {
		super();
	}

	public Passenger(String name, String family, Date birthDate, String email, String phone, String gender,
			String payment) {
		super();
		this.name = name;
		this.family = family;
		this.birthDate = birthDate;
		this.email = email;
		this.phone = phone;
		this.gender = gender;
		this.payment = payment;
	}
	
	
	

	public Passenger(String name, String family, Date birthDate, String email, String phone, String gender,
			String payment, Address address, Flight flight) {
		super();
		this.name = name;
		this.family = family;
		this.birthDate = birthDate;
		this.email = email;
		this.phone = phone;
		this.gender = gender;
		this.payment = payment;
		this.address = address;
		this.flight = flight;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}


	

}
