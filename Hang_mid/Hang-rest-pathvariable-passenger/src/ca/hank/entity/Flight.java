package ca.hank.entity;

import java.util.Date;

public class Flight {

	private String fromCity;
	private Date departureDate;
	
	private String toCity;
	private Date destinationDate;
	
	public Flight() {
		super();
	}

	public Flight(String fromCity, Date departureDate, String toCity, Date destinationDate) {
		super();
		this.fromCity = fromCity;
		this.departureDate = departureDate;
		this.toCity = toCity;
		this.destinationDate = destinationDate;
	}

	public String getFromCity() {
		return fromCity;
	}

	public void setFromCity(String fromCity) {
		this.fromCity = fromCity;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getToCity() {
		return toCity;
	}

	public void setToCity(String toCity) {
		this.toCity = toCity;
	}

	public Date getDestinationDate() {
		return destinationDate;
	}

	public void setDestinationDate(Date destinationDate) {
		this.destinationDate = destinationDate;
	}
	



}
