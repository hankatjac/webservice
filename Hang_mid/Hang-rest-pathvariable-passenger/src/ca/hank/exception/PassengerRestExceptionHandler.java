package ca.hank.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class PassengerRestExceptionHandler {
	@ExceptionHandler
	public ResponseEntity<PassengerErrorResponse> handleExption(PassengerNotFoundException exception) {

		PassengerErrorResponse error = new PassengerErrorResponse();

		error.setStatus(HttpStatus.NOT_FOUND.value());

		error.setMessage(exception.getMessage());

		error.setTimeStamp(System.currentTimeMillis());

		return new ResponseEntity<PassengerErrorResponse>(error, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler
	public ResponseEntity<PassengerErrorResponse> handleExption(Exception exception) {
		PassengerErrorResponse error = new PassengerErrorResponse();

		error.setStatus(HttpStatus.BAD_REQUEST.value());

		error.setMessage(exception.getMessage());

		error.setTimeStamp(System.currentTimeMillis());

		return new ResponseEntity<PassengerErrorResponse>(error, HttpStatus.BAD_REQUEST);
	}


}
