package ca.hank.exception;

public class PassengerNotFoundException extends RuntimeException{

	public PassengerNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PassengerNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PassengerNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	

	
}
