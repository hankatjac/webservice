package ca_hank.jaxbjac;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.example.customer.Customer;



/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		System.out.println("Hello World!");

		try {
			Customer customer = new Customer();
			customer.setId(123);
			customer.setName("Hank");

			JAXBContext context = JAXBContext.newInstance(Customer.class);
			Marshaller marshaller = context.createMarshaller();
			Unmarshaller unMarshaller = context.createUnmarshaller();

			StringWriter writer = new StringWriter();

			// Java object to xml
			marshaller.marshal(customer, writer);
			System.out.println("marshaller (java object to xml): \n" + writer.toString());
			// xml to java object
			Customer customerResult = (Customer) unMarshaller.unmarshal(new StringReader(writer.toString()));
			System.out.println(
					"\nunmarshaller (xml to java object): \n" + customerResult.getId() + ", " + customerResult.getName());
		}

		catch (JAXBException e) {
			System.out.println(e.toString());
		
		}

	}
}
