package hhh;

import java.io.File;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Driver {

	public static void main(String[] args) {
		try {

			ObjectMapper mapper = new ObjectMapper();		
			Customer customerFromJSON = mapper.readValue(new File("data/passenger.json"), Customer.class);
			System.out.println("First name= " + customerFromJSON.getName());
			System.out.println("Last name= " + customerFromJSON.getFamily());
			System.out.println("Street = " + customerFromJSON .getAddress().getStreet());
					
		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}
}