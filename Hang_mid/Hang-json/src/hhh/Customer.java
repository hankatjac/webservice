package hhh;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//ignore unknown properties for mapping in json file
@JsonIgnoreProperties(ignoreUnknown = true)
public class Customer {

	private int id;
	private String name;
	private String family;
	private boolean active;
	private Address address;


	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
