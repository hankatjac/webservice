package ca.hank.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/myrest")
public class DemoRestController {
	@GetMapping("/hello")
	public String sayHello() {

		return "Hello World";
	}
	
	@GetMapping("/hello/date/{date}/city/{city}")
	public String multiplePathVariable(@PathVariable String date, @PathVariable String city) {
		return "Hello! " + date + " " + city;
	}
}
