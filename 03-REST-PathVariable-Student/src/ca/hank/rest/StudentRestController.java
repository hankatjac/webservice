package ca.hank.rest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ca.hank.entity.Student;
import ca.hank.exception.StudentErrorResponse;
import ca.hank.exception.StudentNotFoundException;

@RestController
@RequestMapping("/api")
//@restcontroller will pass list of students to jackson project and jackson will convert it to a json array and then the result will return to user
public class StudentRestController {

	// Define endpoint for "/students" - which returns a list of students
//	@GetMapping("/students")
//	public List<Student> getStudents() {
//		List<Student> theStudents = new ArrayList<>();
//		theStudents.add(new Student("Sheldon", "Couper"));
//		theStudents.add(new Student("Penny", "Rossi"));
//		theStudents.add(new Student("Leonard", "Smith"));
//		return theStudents;
//}

	private List<Student> theStudents;

	@PostConstruct
	public void LoadData() {

		theStudents = new ArrayList<>();

		theStudents.add(new Student("Sheldon", "Couper"));
		theStudents.add(new Student("Penny", "Rossi"));
		theStudents.add(new Student("Leonard", "Smith"));

	}

	// define end point for "/students" - return list of students
	@GetMapping("/students")
	public List<Student> getStudents() {
		return theStudents;
	}

//	@GetMapping("/students/{studentId}")
//	public Student getStudent(@PathVariable int studentId) {
//
//		// 3-throw exception from rest controller method
//
//		if ((studentId >= theStudents.size()) || (studentId < 0)) {
//			throw new StudentNotFoundException("Student Id not found - " + studentId);
//		}
//
//		return theStudents.get(studentId);
//	}

	@GetMapping("/students/{studentId}")
	public Student getStudent(@PathVariable("studentId") String firstName) {

		// 3-throw exception from rest controller method

		for (Student s : theStudents) {
			if (s.getFirstName() != null && s.getFirstName().toUpperCase().contains(firstName.toUpperCase())) {
				return s;
			}

		}
		return null;

	}
}



//	public ResponseEntity<StudentErrorResponse> handleExption(StudentNotFoundException exception) {
//
//		StudentErrorResponse error = new StudentErrorResponse();
//
//		error.setStatus(HttpStatus.NOT_FOUND.value());
//
//		error.setMessage(exception.getMessage());
//
//		error.setTimeStamp(System.currentTimeMillis());
//
//		return new ResponseEntity<StudentErrorResponse>(error, HttpStatus.NOT_FOUND);
//	}
//
//	@ExceptionHandler
//	public ResponseEntity<StudentErrorResponse> handleExption(Exception exception) {
//		StudentErrorResponse error = new StudentErrorResponse();
//
//		error.setStatus(HttpStatus.BAD_REQUEST.value());
//
//		error.setMessage(exception.getMessage());
//
//		error.setTimeStamp(System.currentTimeMillis());
//
//		return new ResponseEntity<StudentErrorResponse>(error, HttpStatus.BAD_REQUEST);
//	}
