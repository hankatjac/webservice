package hhh;

import java.io.File;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Driver {

	public static void main(String[] args) {
		try {

			ObjectMapper mapper = new ObjectMapper();
			//simple json
			// 2- read json file and map/convert to java object from a path: data/sample-lite.json
//			Student studentFromJSON = mapper.readValue(new File("data/sample-lite.json"), Student.class);
			
			
			Student studentFromJSON = mapper.readValue(new File("data/sample-full.json"), Student.class);
			System.out.println("First name= " + studentFromJSON.getFirstName());
			System.out.println("Last name= " + studentFromJSON.getLastName());
			System.out.println("Street = " + studentFromJSON .getAddress().getStreet());
			
			for (String language: studentFromJSON.getLanguages()) {
				System.out.println(language);
			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}
}