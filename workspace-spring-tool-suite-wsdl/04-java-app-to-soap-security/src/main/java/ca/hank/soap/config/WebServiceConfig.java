package ca.hank.soap.config;

import java.util.HashMap;
import java.util.Map;

import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.apache.wss4j.common.ConfigurationConstants;
import org.apache.wss4j.dom.WSConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ca.hank.soap.PaymentProcessorImpl;

@Configuration
public class WebServiceConfig {

	@Autowired
	private Bus bus;

	@Bean
	public Endpoint endpoint() {
		
		EndpointImpl endpoint = new EndpointImpl(bus, new PaymentProcessorImpl());
		endpoint.publish("/paymentProcessor");
		
		// This HashMap will be used for Interceptor configuration
		Map<String, Object> inProps = new HashMap<>();
		
		// Use USERNAME_TOKEN
		inProps.put(ConfigurationConstants.ACTION, ConfigurationConstants.USERNAME_TOKEN);
		
		// PASSWORD_TYPE is TEXT
		inProps.put(ConfigurationConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
		
		// Define the class that going to provide username and password
		inProps.put(ConfigurationConstants.PW_CALLBACK_CLASS, UTPasswordCallback.class.getName());
		
		
		// Create an Interceptor to intercept transaction with the end point
		WSS4JInInterceptor wssIn = new WSS4JInInterceptor(inProps);
		
		// Add the Interceptor to the target end point
		endpoint.getInInterceptors().add(wssIn);
	
		return endpoint;
	}
}