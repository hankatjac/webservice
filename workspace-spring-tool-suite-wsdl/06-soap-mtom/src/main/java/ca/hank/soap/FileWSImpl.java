package ca.hank.soap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

public class FileWSImpl implements FileWS {

	@Override
	public void upload(DataHandler attachment) {

		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			// TODO Auto-generated method stub
			inputStream = attachment.getInputStream();

			outputStream = new FileOutputStream(new File("d:/Customer-Thinking.jpg"));

			byte[] binaryByteArray = new byte[100000];
			int bytesRead = 0;

			while ((bytesRead = inputStream.read(binaryByteArray)) != -1) {

				outputStream.write(binaryByteArray, 0, bytesRead);

			}

		}

		catch (IOException e) {
			e.printStackTrace();

		} finally {
			try {
				inputStream.close();
				outputStream.close();

			} catch (IOException e) {

				e.printStackTrace();
			}
		}

	}

	@Override
	public DataHandler download() {
		// TODO Auto-generated method stub
		return new DataHandler(new FileDataSource(new File("C:/Users/Hank/Pictures/Customer-Thinking.jpg")));
	}

}
