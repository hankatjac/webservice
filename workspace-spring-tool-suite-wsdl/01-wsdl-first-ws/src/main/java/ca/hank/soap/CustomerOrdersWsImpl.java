package ca.hank.soap;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mydomain.ws.trainings.CreateOrdersRequest;
import com.mydomain.ws.trainings.CreateOrdersResponse;
import com.mydomain.ws.trainings.CustomerOrdersPortType;
import com.mydomain.ws.trainings.GetOrdersRequest;
import com.mydomain.ws.trainings.GetOrdersResponse;
import com.mydomain.ws.trainings.Order;
import com.mydomain.ws.trainings.Product;

public class CustomerOrdersWsImpl implements CustomerOrdersPortType {

	// Customer ID is BigInteger from generated classes based on WSDL
	// Each customer has a list of orders
	Map<BigInteger, List<Order>> customerOrders = new HashMap<>();
	// Default value is zero
	int currentId;

	public CustomerOrdersWsImpl() {
		init();
	}

	// This method will initialize a sample order
	public void init() {
		List<Order> orders = new ArrayList<>();
		Order order = new Order();
		// BigInteger.valueOf(1) get a long and return a BigInteger
		order.setId(BigInteger.valueOf(1));
		Product product = new Product();
		product.setId("1");
		product.setDescription("iPhone");
		product.setQuantity(BigInteger.valueOf(3));
		// This is an order of 3 iPhones
		order.getProduct().add(product);
		orders.add(order);
		// Each customer has number of orders. Add current orders to current customer
		customerOrders.put(BigInteger.valueOf(++currentId), orders);
	}
	// This method will get the customer id from the request message and return list
	// of orders for that customer

	@Override
	public GetOrdersResponse getOrders(GetOrdersRequest request) {
		// Retrieve this customer ID from the request
		BigInteger customerId = request.getCustomerId();
		// Get the list of orders for this customer from customerOrders Hashmap
		List<Order> orders = customerOrders.get(customerId);
		// Add orders to response object
		GetOrdersResponse response = new GetOrdersResponse();
		response.getOrder().addAll(orders);
		return response;
	}

	// This method create a new order and add it to the customer orders
	@Override
	public CreateOrdersResponse createOrders(CreateOrdersRequest request) {
		// Retrieve this customer ID from the request
		BigInteger customerId = request.getCustomerId();
		// Get this customer all orders
		Order order = request.getOrder();
		List<Order> orders = customerOrders.get(customerId);
		orders.add(order);
		// Let client knows everything is OK and the job is done
		CreateOrdersResponse response = new CreateOrdersResponse();
		response.setResult(true);
		return response;
	}

}
