package ca.hank.soap;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ca.hank.datatransferobjects.PaymentProcessorRequest;
import ca.hank.datatransferobjects.PaymentProcessorResponse;

@WebService(name = "PaymentProcessor")
public interface PaymentProcessor {
	@WebMethod
	public @WebResult(name = "response") PaymentProcessorResponse processPayment(
			@WebParam(name = "PaymentProcessorRequest") PaymentProcessorRequest paymentProcessorRequest);

}
