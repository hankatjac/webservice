package ca.hank.soap.config;

import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ca.hank.soap.HelloWS;

@Configuration
public class WebServiceConfig {
	/*
	 * The Bus is the backbone of the CXF architecture. It manages extensions and
	 * acts as an interceptor provider. // The interceptors for the bus will be
	 * added to the respective inbound and outbound message and fault interceptor
	 * chains for all client and server endpoints created on the bus (in its
	 * context).
	 */
	@Autowired
	private Bus bus;

	@Bean
	public Endpoint endpoint() {
		Endpoint endpoint = new EndpointImpl(bus, new HelloWS());
		endpoint.publish("/hello");
		return endpoint;
	}
}
