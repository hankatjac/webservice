package ca.hank.passenger;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javassist.expr.NewArray;

@Controller
public class AddressController {

	@Autowired
	private AddressService addressService;

	@RequestMapping("/address")
	public ModelAndView home() {
		List<Address> listAddress = addressService.listAll();
		ModelAndView mav = new ModelAndView("address");
		mav.addObject("listAddress", listAddress);
		return mav;
	}

	@RequestMapping("/address/new")
	public String newAddressForm(Map<String, Object> model) {
		Address address = new Address();
		model.put("address", address);
		return "new_address";
	}

	@RequestMapping(value = "/address/save", method = RequestMethod.POST)
	public String saveAddress(@ModelAttribute("address") Address address) {
		addressService.save(address);
		return "redirect:/";
	}

	@RequestMapping("/address/edit")
	public ModelAndView editAddressForm(@RequestParam long id) {
		ModelAndView mav = new ModelAndView("edit_address");
		Address address = addressService.get(id);
		mav.addObject("address", address);

		return mav;
	}

	@RequestMapping("/address/delete")
	public String deleteAddressForm(@RequestParam long id) {
		addressService.delete(id);
		return "redirect:/";
	}

	@RequestMapping("/address/search")
	public ModelAndView search(@RequestParam String keyword) {
		List<Address> result = addressService.search(keyword);
		ModelAndView mav = new ModelAndView("search_address");
		mav.addObject("result", result);

		return mav;
	}
}
