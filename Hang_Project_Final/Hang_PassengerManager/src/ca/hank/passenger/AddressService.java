package ca.hank.passenger;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AddressService {
	@Autowired
	AddressRepository repo;

	public void save(Address address) {
		repo.save(address);
	}

	public List<Address> listAll() {
		return (List<Address>) repo.findAll();
	}

	public Address get(Long id) {
		return repo.findById(id).get();
	}

	public void delete(Long id) {
		repo.deleteById(id);
	}

	public List<Address> search(String keyword) {
		return repo.search(keyword);
	}
}
