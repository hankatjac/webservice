package ca.hank.passenger;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


public interface AddressRepository extends CrudRepository<Address, Long> {
	@Query(value = "SELECT a FROM Address a WHERE a.street LIKE '%' || :keyword || '%'"
			+ " OR a.city LIKE '%' || :keyword || '%'"
			+ " OR a.state LIKE '%' || :keyword || '%'"
			+ " OR a.country LIKE '%' || :keyword || '%'")
	public List<Address> search(@Param("keyword") String keyword);

}
