package ca.hank.passenger;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface PassengerRepository extends CrudRepository<Passenger, Long> {
	@Query(value = "SELECT p FROM Passenger p WHERE p.name LIKE '%' || :keyword || '%'"
			+ " OR p.family LIKE '%' || :keyword || '%'"
			+ " OR p.email LIKE '%' || :keyword || '%'")
	public List<Passenger> search(@Param("keyword") String keyword);
}
