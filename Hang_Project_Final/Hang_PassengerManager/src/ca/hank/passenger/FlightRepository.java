package ca.hank.passenger;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


public interface FlightRepository extends CrudRepository<Flight, Long> {
	@Query(value = "SELECT f FROM Flight f WHERE f.fromCity LIKE '%' || :keyword || '%'"
			+ " OR f.toCity LIKE '%' || :keyword || '%'")
	
	public List<Flight> search(@Param("keyword") String keyword);

}
