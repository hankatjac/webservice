package ca.hank.passenger;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FlightService {
	@Autowired
	FlightRepository repo;

	public void save(Flight flight) {
		repo.save(flight);
	}

	public List<Flight> listAll() {
		return (List<Flight>) repo.findAll();
	}

	public Flight get(Long id) {
		return repo.findById(id).get();
	}

	public void delete(Long id) {
		repo.deleteById(id);
	}

	public List<Flight> search(String keyword) {
		return repo.search(keyword);
	}
}
