package ca.hank.passenger;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "passenger")
public class Passenger {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long passengerId;
	
	private String name;
	private String family;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date birthDate;
	private String email;
	private String phone;
	private String gender;
	private String payment;
	
	@ManyToOne
	@JoinColumn(name = "addressId", referencedColumnName = "addressId")
	private Address address;
	
	@ManyToOne
	@JoinColumn(name = "flightId", referencedColumnName = "flightId")
	private Flight flight;
	
	public Passenger() {
		super();
	}

	public Passenger(String name, String family, Date birthDate, String email, String phone, String gender,
			String payment) {
		super();
		this.name = name;
		this.family = family;
		this.birthDate = birthDate;
		this.email = email;
		this.phone = phone;
		this.gender = gender;
		this.payment = payment;
	}

	public Long getPassengerId() {
		return passengerId;
	}

	public void setPassengerId(Long passengerId) {
		this.passengerId = passengerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}
	
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}



}
