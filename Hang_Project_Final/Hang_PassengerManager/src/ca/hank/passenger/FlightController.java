package ca.hank.passenger;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class FlightController {

	@Autowired
	private FlightService flightService;

	@RequestMapping("/flight")
	public ModelAndView home() {
		List<Flight> listFlight = flightService.listAll();
		ModelAndView mav = new ModelAndView("flight");
		mav.addObject("listFlight", listFlight);
		return mav;
	}

	@RequestMapping("/flight/new")
	public String newFlightForm(Map<String, Object> model) {
		Flight flight = new Flight();
		model.put("flight", flight);
		return "new_flight";
	}

	@RequestMapping(value = "/flight/save", method = RequestMethod.POST)
	public String saveFlight(@ModelAttribute("flight") Flight flight) {
		flightService.save(flight);
		return "redirect:/";
	}

	@RequestMapping("/flight/edit")
	public ModelAndView editFlightForm(@RequestParam long id) {
		ModelAndView mav = new ModelAndView("edit_flight");
		Flight flight = flightService.get(id);
		mav.addObject("flight", flight);

		return mav;
	}

	@RequestMapping("/flight/delete")
	public String deleteFlightForm(@RequestParam long id) {
		flightService.delete(id);
		return "redirect:/";
	}

	@RequestMapping("/flight/search")
	public ModelAndView search(@RequestParam String keyword) {
		List<Flight> result = flightService.search(keyword);
		ModelAndView mav = new ModelAndView("search_flight");
		mav.addObject("result", result);

		return mav;
	}
}
