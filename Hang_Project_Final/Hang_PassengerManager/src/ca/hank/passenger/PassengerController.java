package ca.hank.passenger;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PassengerController {

	@Autowired
	private PassengerService passengerService;
	
	@RequestMapping("/")
	public ModelAndView home() {
		List<Passenger> listPassenger = passengerService.listAll();
		ModelAndView mav = new ModelAndView("index");
		mav.addObject("listPassenger", listPassenger);
		return mav;
	}
	
	@RequestMapping("/new")
	public String newPassengerForm(Map<String, Object> model) {
		Passenger passenger = new Passenger();
		model.put("passenger", passenger);
		return "new_passenger";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String savePassenger(@ModelAttribute("passenger") Passenger passenger) {
		passengerService.save(passenger);
		return "redirect:/";
	}
	
	@RequestMapping("/edit")
	public ModelAndView editPassengerForm(@RequestParam long id) {
		ModelAndView mav = new ModelAndView("edit_passenger");
		Passenger passenger = passengerService.get(id);
		mav.addObject("passenger", passenger);
		
		return mav;
	}
	
	@RequestMapping("/delete")
	public String deletePassengerForm(@RequestParam long id) {
		passengerService.delete(id);
		return "redirect:/";		
	}
	
	@RequestMapping("/search")
	public ModelAndView search(@RequestParam String keyword) {
		List<Passenger> result = passengerService.search(keyword);
		ModelAndView mav = new ModelAndView("search");
		mav.addObject("result", result);
		
		return mav;		
	}	
}
