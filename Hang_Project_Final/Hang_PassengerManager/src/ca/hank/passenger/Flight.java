package ca.hank.passenger;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "flight")
public class Flight {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long flightId;
	
	private String fromCity;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date departureDate;
	private String toCity;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date destinationDate;
	
	public Flight() {
		super();
	}

	public Flight(String fromCity, Date departureDate, String toCity, Date destinationDate) {
		super();
		this.fromCity = fromCity;
		this.departureDate = departureDate;
		this.toCity = toCity;
		this.destinationDate = destinationDate;
	}

	
	public Long getFlightId() {
		return flightId;
	}

	public void setFlightId(Long flightId) {
		this.flightId = flightId;
	}

	public String getFromCity() {
		return fromCity;
	}

	public void setFromCity(String fromCity) {
		this.fromCity = fromCity;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getToCity() {
		return toCity;
	}

	public void setToCity(String toCity) {
		this.toCity = toCity;
	}

	public Date getDestinationDate() {
		return destinationDate;
	}

	public void setDestinationDate(Date destinationDate) {
		this.destinationDate = destinationDate;
	}
	



}
