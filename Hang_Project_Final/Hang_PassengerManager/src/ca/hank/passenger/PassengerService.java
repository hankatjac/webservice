package ca.hank.passenger;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javassist.tools.web.BadHttpRequest;

@Service
@Transactional
@RestController
@RequestMapping("/api")
public class PassengerService {
	@Autowired
	PassengerRepository repo;
	
	@PostMapping("passengers/add")
	public Passenger create(@RequestBody Passenger passenger) {
		return repo.save(passenger);
	}

	public void save(Passenger passenger) {
		repo.save(passenger);
	}

	@GetMapping("/passengers")
	public List<Passenger> listAll() {
		return (List<Passenger>) repo.findAll();
	}

	@GetMapping(path = "/passengers/{id}")
	public Passenger get(@PathVariable("id") Long id) {
		return repo.findById(id).get();
	}

	@DeleteMapping(path = "/passengers/delete/{id}")
	public void delete(@PathVariable("id") Long id) {
		repo.deleteById(id);
	}
	
	@PutMapping("/passengers/edit/{id}")
	public Passenger updatePassenger(@PathVariable long id, @RequestBody Passenger passenger) throws BadHttpRequest {
		if (repo.existsById(id)) {
            passenger.setName(passenger.getName());
            return repo.save(passenger);
        } else {
            throw new BadHttpRequest();
        }
	}

	public List<Passenger> search(String keyword) {
		return repo.search(keyword);
	}
}
