<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Flight Manager</title>
</head>
<body>
	<div align="center">
		<h2>Flight Manager</h2>
		<form method="get" action="flight/search">
			<input type="text" name="keyword" /> &nbsp; <input type="submit"
				value="Search Flight" />
		</form>
		<h3>
			<a href="flight/new">New Flight</a>
		</h3>
		
		<table border="1" cellpadding="5">
			<tr>
				<th>ID</th>
				<th>From City</th>
				<th>Departure Date</th>
				<th>To City</th>
				<th>Destination Date</th>
				<th>Action</th>
			</tr>
			<c:forEach items="${listFlight}" var="flight">
				<tr>
					<td>${flight.flightId}</td>
					<td>${flight.fromCity}</td>
					<td>${flight.departureDate}</td>
					<td>${flight.toCity}</td>
					<td>${flight.destinationDate}</td>
					<td><a href="flight/edit?id=${flight.flightId}">Edit</a>
						&nbsp;&nbsp;&nbsp; <a href="flight/delete?id=${flight.flightId}">Delete</a>
					</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>