<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New Customer</title>
</head>
<body>
	<div align="center">
		<h2>New Passenger</h2>
		<form:form action="save" method="post" modelAttribute="passenger">
			<table border="0" cellpadding="5">
				<tr>
					<td>Name:</td>
					<td><form:input path="name" /></td>
				</tr>

				<tr>
					<td>Family:</td>
					<td><form:input path="family" /></td>
				</tr>

				<tr>
					<td>Birth Date:</td>
					<td><form:input path="birthDate" /></td>
				</tr>

				<tr>
					<td>Email:</td>
					<td><form:input path="email" /></td>
				</tr>
				<tr>
					<td>Phone:</td>
					<td><form:input path="phone" /></td>
				</tr>

				<tr>
					<td>Gender:</td>
					<td><form:input path="gender" /></td>
				</tr>

				<tr>
					<td>Payment:</td>
					<td><form:input path="payment" /></td>
				</tr>

				<tr>
					<td>Address Id:</td>
					<td><form:input path="address.addressId" /></td>
				</tr>

				<tr>
					<td>Flight Id:</td>
					<td><form:input path="flight.flightId" /></td>
				</tr>

				<tr>
					<td colspan="2"><input type="submit" value="Save"></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>