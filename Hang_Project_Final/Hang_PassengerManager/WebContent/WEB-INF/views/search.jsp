<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Search Result</title>
</head>
<body>
	<div align="center">
		<h2>Search Result</h2>
		<table border="1" cellpadding="5">
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>family</th>
				<th>birth Date</th>
				<th>E-mail</th>
				<th>phone</th>
				<th>gender</th>
				<th>Payment</th>
				<th>Action</th>
			</tr>
			<c:forEach items="${result}" var="passenger">
				<tr>
					<td>${passenger.passengerId}</td>
					<td>${passenger.name}</td>
					<td>${passenger.family}</td>
					<td>${passenger.birthDate}</td>
					<td>${passenger.email}</td>
					<td>${passenger.phone}</td>
					<td>${passenger.gender}</td>
					<td>${passenger.payment}</td>
					<td>
				<a href="edit?id=${passenger.passengerId}">Edit</a>
				&nbsp;&nbsp;&nbsp;
				<a href="delete?id=${passenger.passengerId}">Delete</a>
			</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>