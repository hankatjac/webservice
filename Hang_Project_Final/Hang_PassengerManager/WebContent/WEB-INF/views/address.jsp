<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Address Manager</title>
</head>
<body>
	<div align="center">
		<h2>Address Manager</h2>
		<form method="get" action="address/search">
			<input type="text" name="keyword" /> &nbsp; <input type="submit"
				value="Search Address" />
		</form>
		<h3>
			<a href="address/new">New Address</a>
		</h3>
		
		<table border="1" cellpadding="5">
			<tr>
				<th>ID</th>
				<th>Number</th>
				<th>Street</th>
				<th>City</th>
				<th>State</th>
				<th>Zip</th>
				<th>Country</th>
				<th>Action</th>

			</tr>
			<c:forEach items="${listAddress}" var="address">
				<tr>
					<td>${address.addressId}</td>
					<td>${address.number}</td>
					<td>${address.street}</td>
					<td>${address.city}</td>
					<td>${address.state}</td>
					<td>${address.zip}</td>
					<td>${address.country}</td>
					<td><a href="address/edit?id=${address.addressId}">Edit</a>
						&nbsp;&nbsp;&nbsp; <a href="address/delete?id=${address.addressId}">Delete</a>
					</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>