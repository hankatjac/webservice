<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Passenger Manager</title>
</head>
<body>
<div align="center">
	<h2>Passenger Manager</h2>
	<form method="get" action="search">
		<input type="text" name="keyword" /> &nbsp;
		<input type="submit" value="Search" />
	</form>
	<h3><a href="new">New Passenger</a></h3>
	<h3><a href="address">Show Address Table</a></h3>
	<h3><a href="flight">Show Flight Table</a></h3>
	<table border="1" cellpadding="5">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Family</th>
			<th>Birth Date</th>
			<th>E-mail</th>
			<th>Phone</th>
			<th>Gender</th>
			<th>Payment</th>
			<th>Address</th>
			<th>Flight</th>
			<th>Action</th>
		</tr>
		<c:forEach items="${listPassenger}" var="passenger">
		<tr>
			<td>${passenger.passengerId}</td>
			<td>${passenger.name}</td>
			<td>${passenger.family}</td>
			<td>${passenger.birthDate}</td>
			<td>${passenger.email}</td>
			<td>${passenger.phone}</td>
			<td>${passenger.gender}</td>
			<td>${passenger.payment}</td>
			<td>${passenger.address.addressId}</td>
			<td>${passenger.flight.flightId}</td>
			<td>
				<a href="edit?id=${passenger.passengerId}">Edit</a>
				&nbsp;&nbsp;&nbsp;
				<a href="delete?id=${passenger.passengerId}">Delete</a>
			</td>
		</tr>
		</c:forEach>
	</table>
</div>	
</body>
</html>