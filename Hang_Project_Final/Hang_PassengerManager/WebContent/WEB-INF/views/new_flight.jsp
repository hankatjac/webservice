<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New Flight</title>
</head>
<body>
	<div align="center">
		<h2>New Flight</h2>
		<form:form action="save" method="post" modelAttribute="flight">
			<table border="0" cellpadding="5">
				<tr>
					<td>From City:</td>
					<td><form:input path="fromCity" /></td>
				</tr>

				<tr>
					<td>Departure Date:</td>
					<td><form:input path="departureDate" /></td>
				</tr>

				<tr>
					<td>To City:</td>
					<td><form:input path="toCity" /></td>
				</tr>

				<tr>
					<td>Destination Date:</td>
					<td><form:input path="destinationDate" /></td>
				</tr>

				<tr>
					<td colspan="2"><input type="submit" value="Save"></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>